CREATE_TABLE = """CREATE TABLE phonebook (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255) NOT NULL,
    phone VARCHAR(14) NOT NULL,
    stamp TIMESTAMP
);"""

DROP_TABLE = "DROP TABLE IF EXISTS phonebook"

FIND = """SELECT {selection}
FROM phonebook
WHERE {conditions};"""

GET = """SELECT {selection}
FROM phonebook;
"""

SET = """INSERT INTO phonebook ({columns})
VALUES ({values});"""
