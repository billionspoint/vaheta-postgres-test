from os.path import exists
from flask import Response

class Page:
    """ Return html pages from static folder as
        Flask Response object
    """

    def __init__(self, pagename:str):
        """
            >>> with Page("unreal-page") as p: print(p.data)
            b'404 - not found'
            >>> with Page("index") as p: print(p.data == b'404 - not found')
            False
        """
        self.filename = "static/{0}.html".format(pagename)

    def __enter__(self) -> Response:
        if exists(self.filename):
            with open(self.filename) as file:
                return Response(file.read(),
                            status=200,
                            mimetype="text/html")
        else:
            return Response("404 - not found",
                            status=404,
                            mimetype="text/plain")

    def __exit__(self, *o):
        pass


if __name__ == "__main__":
    import doctest
    doctest.testmod()