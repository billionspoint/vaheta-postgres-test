from flask import Flask, Response, request
from db import PhoneBook
from htmlreader import Page
import json


print("hi log")

app = Flask(__name__, static_folder=None)
PHONE_BOOK = PhoneBook()

@app.route("/")
def root_path():
    PHONE_BOOK.create()
    with Page("index") as p:
        return p

@app.route("/api/add", methods=['POST'])
def api_add():
    name = request.form.get("name")
    telephone = request.form.get("telephone")
    PHONE_BOOK.add(name.strip().capitalize(), telephone)
    with Page("success") as p:
        return p

@app.route("/api/find/<name>")
def api_find(name):
    telephone = PHONE_BOOK.find("name", name.capitalize())
    # data = str(telephone)
    data = json.dumps(telephone, ensure_ascii=False)
    return Response(data, mimetype="application/json")


app.run(host="0.0.0.0", port="8088", debug=True)