Install and run instructions
============================

Description
-----------

Web application for save and find phone number by owner name in a PostgreSQL
server.

Technologies used in project
----------------------------

+ Python3
+ Flask
+ PostgreSQL
+ VueJS
+ Bash
+ Docker

Setup
-----

At first, clone this repository, or just load as zip file and extract.

Make sure you already install Docker. Simplest way to install Docker in any
linux distro &mdash; snap package manager (snapd in default linux repos).
After docker install just run `bash setup/*` in current directory for install
postgres and server container, or manualy install using commands as super
user in bash:

```
# from setup/postgres.bash
docker pull postgres
docker run --name kiselevn-test-db --network host -e POSTGRES_PASSWORD=test -d postgres
docker start kiselevn-test-db

# from setup/python.bash
docker build -t kiselevn-server-img .
docker run --name kiselevn-test-server --network host -d kiselevn-server-img
docker start kiselevn-test-server
```

How to use
----------

After successfully instalation open in browser [localhost:8088](http://localhost:8088).

1. Use "Add new" from for adding new user. 

1. Use "Find" form for searching from existing members.

Uninstallation
--------------

Just stop and remove both containers in Docker.

```
# Stop and uninstall Server
docker stop kiselevn-test-server
docker rm kiselevn-test-server

# Stop and uninstall Database
docker stop kiselevn-test-db
docker rm kiselevn-test-db
```