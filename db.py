import psycopg2
import sqlcommands
from datetime import datetime


class DataBaseException(Exception):
    """ PostgreSQL database class exceptions
    """
    pass


class DataBase:
    """ PostgreSQL database class
    """

    def __init__(self, host:str='localhost', user:str="postgres", password:str="test"):
        """
            >>> testdb = DataBase()
            >>> type(testdb) == DataBase
            True
        """
        try:
            # Connect to database
            dbident = "user='{0}' host='{1}' password='{2}'".format(user, host, password)
            self.connection = psycopg2.connect(dbident)
        except psycopg2.OperationalError:
            # Exception if Docker has not setuped
            raise DataBaseException("Cant setup connection. Please check Postgres Docker container")
        pass

    def execute(self, command:str) -> list:
        """
            >>> testdb = DataBase()
            >>> type(testdb.execute('SELECT * FROM pg_database'))
            <class 'list'>
        """
        try:
            cursor = self.connection.cursor()
            cursor.execute(command)
            results = []
            try:
                results = cursor.fetchall()
            except:
                self.connection.commit()
            cursor.close()
            return results
        except psycopg2.InternalError:
            self.connection.rollback()
            self.execute(command)


class PhoneBook(DataBase):
    """ DataBase of phones
    """
    def __init__(self):
        """
            >>> testdb = PhoneBook()
            >>> type(testdb) == PhoneBook
            True
        """
        # Inherit SQL database class
        super().__init__()

    def force_create(self):
        """
            >>> testdb = PhoneBook()
            >>> testdb.force_create()
        """
        self.execute(sqlcommands.DROP_TABLE)
        self.execute(sqlcommands.CREATE_TABLE)

    def create(self):
        """
            >>> testdb = PhoneBook()
            >>> testdb.create()
        """
        try:
            self.execute(sqlcommands.CREATE_TABLE)
        except Exception:
            self.connection.rollback()

    def add(self, name:str, phone:str):
        stamp = datetime.now()
        columns = "name, phone, stamp"
        values = "'{0}', '{1}', '{2}'".format(name, phone, stamp)
        self.execute(sqlcommands.SET.format(columns=columns,
                                            values=values))

    def find(self, column:str, value:str) -> list:
        selection = "name, phone, stamp"
        conditions =  "{0} LIKE '%{1}%'".format(column, value)
        rows = self.execute(sqlcommands.FIND.format(selection=selection,
                                                    conditions=conditions))
        print(rows)
        for i, row in enumerate(rows):
            normalized_row = (row[0], row[1], row[2].strftime("%d.%m.%Y %H:%M:%S"))
            rows[i] = normalized_row
        return rows


if __name__ == "__main__":
    import doctest
    doctest.testmod()