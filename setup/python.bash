#!/bin/bash

sudo docker build -t kiselevn-server-img . ; \

sudo docker stop kiselevn-test-server ; \

sudo docker rm kiselevn-test-server ; \

sudo docker run --name kiselevn-test-server --network host -d kiselevn-server-img && \

sudo docker start kiselevn-test-server && \

# tput setaf 1 && \
# echo Error in Python3 container setup && \
# tput sgr 0 && \

# echo && \
# exit 1;

python3 -m webbrowser http://localhost:8088;