#!/bin/bash

sudo docker pull postgres && \

sudo docker stop kiselevn-test-db ; \

sudo docker rm kiselevn-test-db ; \

sudo docker run --name kiselevn-test-db --network host -e POSTGRES_PASSWORD=test -d postgres && \

sudo docker start kiselevn-test-db ; \

# tput setaf 1 && \
# echo Error in postgres install && \
# tput sgr 0 && \

# echo && \
# exit 1;